/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import annotation.*;
import annot.*;
import modelV.ModelView;
/**
 *
 * @author USER
 */
@NomTable(nom="Directeur")
public class Directeur {
    @Attribut(isprimarykey=true,columnName="idDirecteur")
    String idDirecteur;
    @Attribut(columnName="nom")
    String nom;
    @Attribut(columnName="prenom")
    String prenom;

    public Directeur() {
        
    }

    public Directeur(String idDirecteur, String nom, String prenom) {
        this.idDirecteur = idDirecteur;
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getIdDirecteur() {
        return this.idDirecteur;
    }

    public void setIdDirecteur(String idDirecteur) {
        this.idDirecteur = idDirecteur;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    @Url("listeDire")
    public  ModelView listeDirecteur(){
        ModelView retour=new ModelView();
        HashMap<String,Object> ale=new HashMap<String,Object>();
        ale.put("test", "directeur");
        ArrayList<Directeur> dir=new ArrayList<Directeur>();
        Directeur a=new Directeur();
        a.setNom("Alain");
        a.setPrenom("duke");
        dir.add(a);
        ale.put("liste",dir);
        retour.setUrldispatch("resultat.jsp");
        retour.setAttributes(ale);

        return retour;
    }
   public String getId(){
       return "d";
   }
   
}
