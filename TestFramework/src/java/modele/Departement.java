package modele;
import DAO.GenericDao;
import annot.Url;
import annotation.*;
import database.Connexion;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import modelV.ModelView;
@NomTable(nom="Departement")
public class Departement{
    @Attribut(isprimarykey=true,columnName="idDepartement")
    String idDepart;
    @Attribut(columnName="intitule")
    String intitule;

    public Departement() {
    }

    public Departement(String idDepartement, String intitule) {
        this.idDepart = idDepartement;
        this.intitule = intitule;
    }

    public String getIdDepart() {
        return this.idDepart;
    }

    public void setIdDepart(String idDepartement) {
        this.idDepart = idDepartement;
    }

    public String getIntitule() {
        return this.intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }
    public String getId(){
        return "d";
    }
    @Url("listeDept")
   public  ModelView listeDept()throws Exception{
       ModelView retour=new ModelView();
       HashMap<String,Object> ale=new HashMap<String,Object>();
       ale.put("test", "departement");
       ArrayList<Departement> dir=findAll();
       ale.put("liste",dir);
       retour.setUrldispatch("resultat2.jsp");
       retour.setAttributes(ale);
       
       return retour;
   }
   @Url("insert-dept")
   public ModelView save()throws Exception{
       Connection ray=null;
       try{
           ray=Connexion.getConnection("jdbc:postgresql://localhost:5432/test","postgres","mahary");
       }catch(Exception e){
           
       }
       ModelView retour=new ModelView();
       HashMap<String,Object> ale=new HashMap<String,Object>();
       ale.put("nom", this.intitule);
       GenericDao.insert(this,ray,"postgres");
       retour.setUrldispatch("recept.jsp");
       retour.setAttributes(ale);
       if(ray!=null){
           ray.close();
       }
       return retour;
   }
   public ArrayList<Departement> findAll()throws Exception{
       ArrayList<Departement> dir=new ArrayList<Departement>();
       Connection ray=null;
       try{
           ray=Connexion.getConnection("jdbc:postgresql://localhost:5432/test","postgres","mahary");
       }catch(Exception e){
           
       }
       Object[]ra=GenericDao.select(new Departement(null,null),ray);
       if(ra.length>0){
           for(int i=0;i<ra.length;i++){
               dir.add((Departement)ra[i]);
            }
       }
       if(ray!=null){
           ray.close();
       }
       return dir;
       
   }
   

}