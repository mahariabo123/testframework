package modele;
import DAO.GenericDao;
import annot.Url;
import annotation.*;
import database.Connexion;
import java.sql.Connection;
import java.util.HashMap;
import modelV.ModelView;
@NomTable(nom="Employe")
public class Employe{
    @Attribut(isprimarykey=true,columnName="idEmploye")
    String idEmploye;
    @Attribut(columnName="nom")
    String nom;
    @Attribut(columnName="prenom")
    String prenom;
    @Attribut(columnName="idDept",isforeignkey=true)
    @Foreignkey(columnRef="idDepartement")
    Departement dept;

    public Employe() {
    }

    public Employe(String idEmploye, String nom, String prenom, Departement dept) {
        this.idEmploye = idEmploye;
        this.nom = nom;
        this.prenom = prenom;
        this.dept = dept;
    }

    public String getIdEmploye() {
        return this.idEmploye;
    }

    public void setIdEmploye(String idEmploye) {
        this.idEmploye = idEmploye;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Departement getDept() {
        return this.dept;
    }

    public void setDept(Object dept) {
        Departement ray=null;
        if(dept instanceof Departement){
            ray=(Departement)dept;
            this.dept = ray;
        }
        
    }
    @Url("insert-emp")
   public ModelView save()throws Exception{
       Connection ray=null;
       try{
           ray=Connexion.getConnection("jdbc:postgresql://localhost:5432/test","postgres","mahary");
       }catch(Exception e){
           
       }
       ModelView retour=new ModelView();
       HashMap<String,Object> ale=new HashMap<String,Object>();
       ale.put("nom", this.nom);
       GenericDao.insert(this,ray,"postgres");
       retour.setUrldispatch("recept.jsp");
       retour.setAttributes(ale);
       if(ray!=null){
           ray.close();
       }
       return retour;
   }
   public String getId(){
       return "e";
   }

}